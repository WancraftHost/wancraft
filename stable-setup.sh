#https://github.com/Nincraft/ModPackDownloader/releases
apt update
add-apt-repository -y ppa:openjdk-r/ppa
apt update
apt-get install -y openjdk-11-jre-headless
wget https://github.com/Nincraft/ModPackDownloader/releases/download/0.6.1/ModpackDownloader-cli-0.6.1.jar
echo "STAGE 1: DOWNLOAD MODS"
mv ModpackDownloader-cli-0.6.1.jar mpdl.jar
java -jar mpdl.jar -folder mods
echo "STAGE 2: PACKAGE FILES"
zip -r "wancraft-stable.zip" bin config mods
echo "Done"
curl -v -s -X POST https://${my_encryption}@api.bitbucket.org/2.0/repositories/WancraftHost/wancraft/downloads -F files=@wancraft-stable.zip
echo "Build complete."