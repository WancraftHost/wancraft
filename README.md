# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for a modpack I made for some friends.
* 1.0

## How do I get set up? ##

### Summary of set up ###
To get a copy of this modpack from the source itself, run dev-setup.sh in a bash shell on a system with the zip command available.

## Contribution guidelines ##
If upon testing the dev build your code does not run, your merge request will be declined.